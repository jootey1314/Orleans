﻿using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;

namespace MongoDBHelper
{
    public class Base
    {
        #region DB配置

        //数据库连接字符串
        private static string strconn = "mongodb://sa:11deyicomdev@127.0.0.1:10001";

        //数据库名称
        private string dbName = "OrleansMogon";
        #endregion
        #region  初始化


        private IMongoDatabase db = null;
        //创建数据库链接        
        private MongoClient server = new MongoClient(strconn);//.GetServer();// MongoServer.Create(strconn);
        public string CurrentDoc { set; get; }//当前操作的集合名称（collections）



        public IMongoCollection<BsonDocument> Collection;//当前操作集合实体
        public Base()
        {
            //获得数据库cnblogs
            db = server.GetDatabase(dbName);
            //server.Connect();
        }
        /// <summary>
        /// 切换操作集合类
        /// </summary>
        /// <param name="collection"></param>
        public void SwitchCollection(string collection)
        {
            this.CurrentDoc = collection;
            this.Init();
        }
        /// <summary>
        /// 泛型函数，根据数据类名来却换全局collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="className"></param>
        public void SwitchCollectionByClassType<T>()
        {
            var StrClassName = typeof(T).Name.ToString();
            if (StrClassName.ToLower() == "string")
            {
                return;
            }
            this.SwitchCollection(StrClassName);
        }
        public void Init()
        {
            Collection = db.GetCollection<BsonDocument>(CurrentDoc);
        }
        //db中是否存在名为xxx的collection
        public bool IsExistForCollection(string collectionName)
        {
            ListCollectionsOptions op = new ListCollectionsOptions { Filter = new BsonDocument { { "name", collectionName } } };
            var CollectionCount = db.ListCollections(op).ToList().Count();
            if (CollectionCount == 0)
            {
                return false; //不存在
            }
            return true;//存在
        }
        #endregion
    }
    public class MongoDbHelper : Base
    {
        private string _CollectionName;
        public string CollectionName
        {
            set
            {
                _CollectionName = value;
                this.CurrentDoc = _CollectionName;
                base.Init();
            }
            get { return _CollectionName; }
        }
        //public MongoDbHelper()
        //{
        //    base.Init();
        //}

        //static MongoDbHelper com;
        //public new static MongoDbHelper Init()
        //{
        //    if (com == null)
        //    {
        //        com = new MongoDbHelper();
        //    }
        //    return com;
        //}

    }
}
