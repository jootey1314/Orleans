﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using IGrain;
using Orleans;
using WebApi.Services.Imp;

namespace WebApi.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index()
        {
            var path = GetAssemblyPath();
            var configFilePath = Path.Combine(path, "ClientConfiguration.xml");

            GrainClient.Initialize(configFilePath);
            var playerId = "123";
            IGameGrain player = GrainClient.GrainFactory.GetGrain<IGameGrain>(playerId);


            //await player.JoinGame(player);
            //var key = player.GetPrimaryKeyString();
            //var shu = player.GetName("dddddd");

            //ViewBag.Title = "Home Page" + key + ":" + shu.Result;



            //首先创建grain的引用
            var friend = GrainClient.GrainFactory.GetGrain<IObserverGrain>("HELLO Obser");
            Chart c = new Chart();

            //为chat创建一个用来订阅可观察的grain的引用。
            var obj = await GrainClient.GrainFactory.CreateObjectReference<IChat>(c);
            //订阅这个实例来接受消息。
            await friend.Subscribe(obj);

            await friend.SendUpdateMessage("hello this is a test");

            return View();
        }

        private static string GetAssemblyPath()
        {
            var codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var codeBaseUri = new UriBuilder(codeBase);
            var pathString = Uri.UnescapeDataString(codeBaseUri.Path);
            return Path.GetDirectoryName(pathString);
        }




    }
}
