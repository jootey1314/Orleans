﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using IGrain;
using Orleans;
using Send.MQ;
namespace WebApi.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            var op = new Send.MQ.Send();
            op.PublishMsg(new [] {"ddd", "fff"});
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public async Task< string> Get(int id)
        {
            var friend = GrainClient.GrainFactory.GetGrain<IMongoGrain>("tcp");
            var dd = await friend.Save("萱萱");
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
