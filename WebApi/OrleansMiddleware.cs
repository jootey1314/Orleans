﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Reflection;
//using System.Web;

//namespace WebApi
//{
//    public static class OrleansMiddleware
//    {
//        /// <summary>
//        /// Configures Orleans for use with Azure.
//        /// </summary>
//        /// <remarks>
//        /// This should appear earlier in the pipeline than any usage of Orleans.
//        /// </remarks>
//        /// <param name="app">The app being configured.</param>
//        public static void ConfigureOrleans(this IAppBuilder app)
//        {
//            var path = GetAssemblyPath();
//            var configFilePath = Path.Combine(path, "ClientConfiguration.xml");
//            var clientConfigFile = new FileInfo(configFilePath);
//            if (!clientConfigFile.Exists)
//            {
//                throw new FileNotFoundException(
//                    string.Format(
//                        "Cannot find Orleans client config file at {0}",
//                        clientConfigFile.FullName),
//                    clientConfigFile.FullName);
//            }
//            app.Use(
//                async (context, next) =>
//                {
//                    if (!OrleansAzureClient.IsInitialized)
//                    {
//                        OrleansAzureClient.Initialize(clientConfigFile);
//                    }

//                    await next.Invoke();
//                });
//        }

//        /// <summary>
//        /// Returns the path of the executing assembly.
//        /// </summary>
//        /// <returns>The path of the executing assembly.</returns>
//        private static string GetAssemblyPath()
//        {
//            var codeBase = Assembly.GetExecutingAssembly().CodeBase;
//            var codeBaseUri = new UriBuilder(codeBase);
//            var pathString = Uri.UnescapeDataString(codeBaseUri.Path);
//            return Path.GetDirectoryName(pathString);
//        }
//    }
//}