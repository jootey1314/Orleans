﻿using System;
using ClientConsole;

namespace WebApi.Services.Imp
{
    public class Chart : IChat
    {
        public void ReceiveMessage(string message)
        {
            LogHelper.WriteLog(typeof(Chart), message);
            Console.WriteLine(message);
        }
    }
}