﻿using System;

namespace ClientConsole.Services.Imp
{
    public class Chart : IChat
    {
        public void ReceiveMessage(string message)
        {
            Console.WriteLine("received message:"+message);
            LogHelper.WriteLog(typeof(Chart), message);
        }
    }
}