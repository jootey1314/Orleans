﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using ClientConsole.Services.Imp;
using IGrain;
using Orleans;

namespace ClientConsole
{
    class Program
    {
        static  void Main(string[] args)
        {
            var path = GetAssemblyPath();
            var configFilePath = Path.Combine(path, "ClientConfiguration.xml");
            GrainClient.Initialize(configFilePath);
            //Done();
            //Insert();
            StateGrain().GetAwaiter();
            Console.ReadLine();
        }


        public static async Task Done()
        {
            //首先创建grain的引用
            var friend = GrainClient.GrainFactory.GetGrain<IObserverGrain>("tcp");
            Chart c = new Chart();

            //为chat创建一个用来订阅可观察的grain的引用。
            var obj = await GrainClient.GrainFactory.CreateObjectReference<IChat>(c);
            //订阅这个实例来接受消息。
            await friend.Subscribe(obj); //订阅了以后，将调用Chart中的方法进行数据处理
        }


        public static async Task Insert()
        {
            var friend = GrainClient.GrainFactory.GetGrain<IMongoGrain>("tcp");
          var dd= await friend.Save("萱萱");

        }


        public static async Task StateGrain()
        {
            var State = GrainClient.GrainFactory.GetGrain<IStateGrain>("state");
            //await State.StateSave();
            var state =await State.GetState();
            Console.WriteLine(state);
            Console.ReadLine();
        }

        private static string GetAssemblyPath()
        {
            var codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var codeBaseUri = new UriBuilder(codeBase);
            var pathString = Uri.UnescapeDataString(codeBaseUri.Path);
            return Path.GetDirectoryName(pathString);
        }
    }
}
