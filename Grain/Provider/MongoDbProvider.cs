﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Grain.Provider
{
    public abstract class MongoDbProvider
    {
        //private readonly MongoClient _Client;
        //private readonly IMongoDatabase _Db;
        /// <summary>
        /// 获取collection集合对象
        /// </summary>
        protected readonly IMongoCollection<BsonDocument> _Collection;
        public abstract string _CollecitonName { get; }

        protected MongoDbProvider()
        {
            var client = new MongoClient();
            var db = client.GetDatabase("Orleans");
            _Collection = db.GetCollection<BsonDocument>(_CollecitonName);
        }
    }
}
