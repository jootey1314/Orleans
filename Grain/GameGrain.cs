using System;
using System.Threading.Tasks;
using IGrain;
using IGrain.Model;
using Orleans;
namespace Grain
{
    /// <summary>
    /// Grain implementation class Grain1.
    /// </summary>
    public class GameGrain : Orleans.Grain, IGameGrain
    {
        private IGameGrain _currentGame;

        // 目前在在线的游戏玩家。可能为null。
        public Task<IGameGrain> GetCurrentGame()
        {
            return Task.FromResult(_currentGame);
        }

        public Task<string> GetName(string name)
        {
            Console.WriteLine("Player {0}", name);
            return Task.FromResult("GetName:"+name);
        }

        public async Task GreetingAsync(GreetingData data)
        {
            Console.WriteLine("{0} said: {1}", data.From, data.Message);

            // stop this from repeating endlessly
            if (data.Count >= 3) return;

            // send a message back to the sender
            var fromGrain = GrainFactory.GetGrain<IEmployee>(data.From);
            await fromGrain.Greeting(new GreetingData
            {
                From = this.GetPrimaryKey(),
                Message = "Thanks!",
                Count = data.Count + 1
            });
        }

        // 游戏grain调用这个方法，来通知有玩家加入了游戏。
        public Task JoinGame(IGameGrain game)
        {
            _currentGame = game;
            Console.WriteLine("Player {0} joined game {1}", this.GetPrimaryKey(), game.GetPrimaryKey());
            return TaskDone.Done;
        }

        // 游戏grain调用这个方法，来通知有玩家离开了游戏。
        public Task LeaveGame(IGameGrain game)
        {
            _currentGame = null;
            Console.WriteLine("Player {0} left game {1}", this.GetPrimaryKey(), game.GetPrimaryKey());
            return TaskDone.Done;
        }



        

    }

}

