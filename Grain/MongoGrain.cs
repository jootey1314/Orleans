﻿using System.Threading.Tasks;
using Grain.Provider;
using Grain.State;
using IGrain;
using MongoDB.Bson;
using Orleans.Providers;

namespace Grain
{
    //连接串在Host的配置文件中
    public class AccountService : MongoDbProvider
    {
        public override string _CollecitonName => "Order";
        public async Task<string> Save(string name)
        {
            await _Collection.InsertOneAsync(new BsonDocument { { "name", name } });
            return "新增成功";
        }
    }



    /// <summary>
    /// Grain implementation class MongoGrain.
    /// </summary>
    [StorageProvider(ProviderName = "MongoDBStorage")]
    public class MongoGrain : Orleans.Grain<UserGrainState>, IMongoGrain
    {
        public async Task<string> Save(string name)
        {
            var op = new AccountService();
            var response = await op.Save(name);
            this.State.Name = name;
            return response;
        }
    }
}
