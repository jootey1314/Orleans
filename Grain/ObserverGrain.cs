﻿using IGrain;
using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orleans.Concurrency;
using WebApi.Services.Imp;

namespace Grain
{
    [Reentrant]//可重入，表示可以供多个Actor调用，彼此之间数据将互不影响
    class ObserverGrain : Orleans.Grain, IObserverGrain
    {
        private ObserverSubscriptionManager<IChat> _subsManager;

        public override async Task OnActivateAsync()
        {
            // We created the utility at activation time.
            _subsManager = new ObserverSubscriptionManager<IChat>();
            await base.OnActivateAsync();
        }

        // Clients call this to subscribe.
        public Task Subscribe(IChat observer)
        {
            _subsManager.Subscribe(observer);
            return TaskDone.Done;
        }

        //Also clients use this to unsubscribe themselves to no longer receive the messages.
        public Task UnSubscribe(IChat observer)
        {
            _subsManager.Unsubscribe(observer);
            return TaskDone.Done;
        }

        public Task SendUpdateMessage(string message)
        {
            message = "message from :" + this.GetPrimaryKeyString() + ":" + message;
            _subsManager.Notify(s => s.ReceiveMessage(message));
            return TaskDone.Done;
        }
    }
}
