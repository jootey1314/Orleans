using System;
using System.Threading.Tasks;
using IGrain;
using Orleans;
using Orleans.Providers;

namespace Grain
{
    public class MyGrainState
    {
        public int Field1 { get; set; }
        public string Field2 { get; set; }
    }


    /// <summary>
    /// Grain implementation class StateGrain.
    /// </summary>
    /// 
    [StorageProvider(ProviderName = "MongoDBStorage")]
    public class StateGrain : Orleans.Grain<MyGrainState>, IStateGrain
    {


        // TODO: replace placeholder grain interface with actual grain
        // communication interface(s).
        public Task StateSave()
        {
            State.Field2 = "jooper";
            base.WriteStateAsync();
            return TaskDone.Done;
        }
        public Task<string> GetState()
        {
            var state = State.Field2;
            return Task.FromResult<string>(state);
        }

    }
}
