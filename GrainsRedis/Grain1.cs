using System.Threading.Tasks;
using IGrainRedis;
using Orleans;
using Orleans.Providers;

namespace GrainsRedis
{
    /// <summary>
    /// Grain implementation class Grain1.
    /// </summary>
    
    [StorageProvider(ProviderName = "RedisStore")]
    public class Grain1 : Grain, IGrain1
    {

    }
}
