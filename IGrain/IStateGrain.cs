using System.Threading.Tasks;
using Orleans;

namespace IGrain
{
    /// <summary>
    /// Grain interface IStateGrain
    /// </summary>
    public interface IStateGrain : IGrainWithStringKey
    {
        Task StateSave();

        Task<string> GetState();
    }
}
