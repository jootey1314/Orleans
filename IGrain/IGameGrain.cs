using System.Threading.Tasks;
using IGrain.Model;
using Orleans;

namespace IGrain
{
    /// <summary>
    /// Grain interface IGrain1
    /// </summary>
    public interface IGameGrain : IGrainWithStringKey
    {
        Task<IGameGrain> GetCurrentGame();
        Task JoinGame(IGameGrain game);
        Task LeaveGame(IGameGrain game);

        Task<string> GetName(string name);

        Task GreetingAsync(GreetingData data);
    }
}
