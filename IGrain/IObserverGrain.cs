﻿using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGrain
{
    public interface IObserverGrain : IGrainWithStringKey
    {
        Task Subscribe(IChat observer);
        Task UnSubscribe(IChat observer);
        Task SendUpdateMessage(string message);
    }
}
