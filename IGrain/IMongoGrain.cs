using System.Threading.Tasks;
using Orleans;

namespace IGrain
{
    /// <summary>
    /// Grain interface IMongoGrain
    /// </summary>
    public interface IMongoGrain : IGrainWithStringKey
    {
        Task<string> Save(string name);
    }
}
