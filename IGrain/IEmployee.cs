﻿using System.Threading.Tasks;
using IGrain.Model;
using Orleans;

namespace IGrain
{
    public interface IEmployee : IGrainWithGuidKey
    {
        Task<int> GetLevel();
        Task Promote(int newLevel);
        Task Greeting(GreetingData data);

    }
}
