#if !EXCLUDE_CODEGEN
#pragma warning disable 162
#pragma warning disable 219
#pragma warning disable 414
#pragma warning disable 649
#pragma warning disable 693
#pragma warning disable 1591
#pragma warning disable 1998
[assembly: global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.4.0.0")]
[assembly: global::Orleans.CodeGeneration.OrleansCodeGenerationTargetAttribute("IGrain, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null")]
namespace Generated8D4E4B6371543BD
{
    using global::Orleans.Async;
    using global::Orleans;
    using global::System.Reflection;

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.4.0.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (IChat))]
    internal class OrleansCodeGenChatReference : global::Orleans.Runtime.GrainReference, IChat
    {
        protected @OrleansCodeGenChatReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenChatReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return -663892786;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "IChat";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == -663892786;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case -663892786:
                    switch (@methodId)
                    {
                        case 886256324:
                            return "ReceiveMessage";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -663892786 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public void @ReceiveMessage(global::System.String @message)
        {
            base.@InvokeOneWayMethod(886256324, new global::System.Object[]{@message});
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.4.0.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute(typeof (IChat), -663892786), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenChatMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            if (@grain == null)
                throw new global::System.ArgumentNullException("grain");
            switch (interfaceId)
            {
                case -663892786:
                    switch (methodId)
                    {
                        case 886256324:
                            ((IChat)@grain).@ReceiveMessage((global::System.String)arguments[0]);
                            return global::Orleans.Async.TaskUtility.@Completed();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -663892786 + ",methodId=" + methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return -663892786;
            }
        }
    }
}

namespace IGrain
{
    using global::Orleans.Async;
    using global::Orleans;
    using global::System.Reflection;

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.4.0.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (global::IGrain.IEmployee))]
    internal class OrleansCodeGenEmployeeReference : global::Orleans.Runtime.GrainReference, global::IGrain.IEmployee
    {
        protected @OrleansCodeGenEmployeeReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenEmployeeReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return -29721547;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "global::IGrain.IEmployee";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == -29721547;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case -29721547:
                    switch (@methodId)
                    {
                        case 312402437:
                            return "GetLevel";
                        case -1461035780:
                            return "Promote";
                        case 502068619:
                            return "Greeting";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -29721547 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public global::System.Threading.Tasks.Task<global::System.Int32> @GetLevel()
        {
            return base.@InvokeMethodAsync<global::System.Int32>(312402437, null);
        }

        public global::System.Threading.Tasks.Task @Promote(global::System.Int32 @newLevel)
        {
            return base.@InvokeMethodAsync<global::System.Object>(-1461035780, new global::System.Object[]{@newLevel});
        }

        public global::System.Threading.Tasks.Task @Greeting(global::IGrain.Model.GreetingData @data)
        {
            return base.@InvokeMethodAsync<global::System.Object>(502068619, new global::System.Object[]{@data});
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.4.0.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute(typeof (global::IGrain.IEmployee), -29721547), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenEmployeeMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            if (@grain == null)
                throw new global::System.ArgumentNullException("grain");
            switch (interfaceId)
            {
                case -29721547:
                    switch (methodId)
                    {
                        case 312402437:
                            return ((global::IGrain.IEmployee)@grain).@GetLevel().@Box();
                        case -1461035780:
                            return ((global::IGrain.IEmployee)@grain).@Promote((global::System.Int32)arguments[0]).@Box();
                        case 502068619:
                            return ((global::IGrain.IEmployee)@grain).@Greeting((global::IGrain.Model.GreetingData)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -29721547 + ",methodId=" + methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return -29721547;
            }
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.4.0.0"), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.SerializerAttribute(typeof (global::IGrain.Model.GreetingData))]
    internal class OrleansCodeGenIGrain_Model_GreetingDataSerializer
    {
        [global::Orleans.CodeGeneration.CopierMethodAttribute]
        public static global::System.Object DeepCopier(global::System.Object original, global::Orleans.Serialization.ICopyContext context)
        {
            // No deep copy required since IGrain.Model.GreetingData is marked with the [Immutable] attribute.
            return original;
        }

        [global::Orleans.CodeGeneration.SerializerMethodAttribute]
        public static void Serializer(global::System.Object untypedInput, global::Orleans.Serialization.ISerializationContext context, global::System.Type expected)
        {
            global::IGrain.Model.GreetingData input = (global::IGrain.Model.GreetingData)untypedInput;
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Count, context, typeof (global::System.Int32));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@From, context, typeof (global::System.Guid));
            global::Orleans.Serialization.SerializationManager.@SerializeInner(input.@Message, context, typeof (global::System.String));
        }

        [global::Orleans.CodeGeneration.DeserializerMethodAttribute]
        public static global::System.Object Deserializer(global::System.Type expected, global::Orleans.Serialization.IDeserializationContext context)
        {
            global::IGrain.Model.GreetingData result = new global::IGrain.Model.GreetingData();
            context.@RecordObject(result);
            result.@Count = (global::System.Int32)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.Int32), context);
            result.@From = (global::System.Guid)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.Guid), context);
            result.@Message = (global::System.String)global::Orleans.Serialization.SerializationManager.@DeserializeInner(typeof (global::System.String), context);
            return (global::IGrain.Model.GreetingData)result;
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.4.0.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (global::IGrain.IGameGrain))]
    internal class OrleansCodeGenGameGrainReference : global::Orleans.Runtime.GrainReference, global::IGrain.IGameGrain
    {
        protected @OrleansCodeGenGameGrainReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenGameGrainReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return -1181533925;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "global::IGrain.IGameGrain";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == -1181533925 || @interfaceId == -1277021679;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case -1181533925:
                    switch (@methodId)
                    {
                        case -210510288:
                            return "GetCurrentGame";
                        case 1908871624:
                            return "JoinGame";
                        case -1586169980:
                            return "LeaveGame";
                        case -1594960074:
                            return "GetName";
                        case -145706865:
                            return "GreetingAsync";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1181533925 + ",methodId=" + @methodId);
                    }

                case -1277021679:
                    switch (@methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public global::System.Threading.Tasks.Task<global::IGrain.IGameGrain> @GetCurrentGame()
        {
            return base.@InvokeMethodAsync<global::IGrain.IGameGrain>(-210510288, null);
        }

        public global::System.Threading.Tasks.Task @JoinGame(global::IGrain.IGameGrain @game)
        {
            return base.@InvokeMethodAsync<global::System.Object>(1908871624, new global::System.Object[]{@game is global::Orleans.Grain ? @game.@AsReference<global::IGrain.IGameGrain>() : @game});
        }

        public global::System.Threading.Tasks.Task @LeaveGame(global::IGrain.IGameGrain @game)
        {
            return base.@InvokeMethodAsync<global::System.Object>(-1586169980, new global::System.Object[]{@game is global::Orleans.Grain ? @game.@AsReference<global::IGrain.IGameGrain>() : @game});
        }

        public global::System.Threading.Tasks.Task<global::System.String> @GetName(global::System.String @name)
        {
            return base.@InvokeMethodAsync<global::System.String>(-1594960074, new global::System.Object[]{@name});
        }

        public global::System.Threading.Tasks.Task @GreetingAsync(global::IGrain.Model.GreetingData @data)
        {
            return base.@InvokeMethodAsync<global::System.Object>(-145706865, new global::System.Object[]{@data});
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.4.0.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute(typeof (global::IGrain.IGameGrain), -1181533925), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenGameGrainMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            if (@grain == null)
                throw new global::System.ArgumentNullException("grain");
            switch (interfaceId)
            {
                case -1181533925:
                    switch (methodId)
                    {
                        case -210510288:
                            return ((global::IGrain.IGameGrain)@grain).@GetCurrentGame().@Box();
                        case 1908871624:
                            return ((global::IGrain.IGameGrain)@grain).@JoinGame((global::IGrain.IGameGrain)arguments[0]).@Box();
                        case -1586169980:
                            return ((global::IGrain.IGameGrain)@grain).@LeaveGame((global::IGrain.IGameGrain)arguments[0]).@Box();
                        case -1594960074:
                            return ((global::IGrain.IGameGrain)@grain).@GetName((global::System.String)arguments[0]).@Box();
                        case -145706865:
                            return ((global::IGrain.IGameGrain)@grain).@GreetingAsync((global::IGrain.Model.GreetingData)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1181533925 + ",methodId=" + methodId);
                    }

                case -1277021679:
                    switch (methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return -1181533925;
            }
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.4.0.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (global::IGrain.IObserverGrain))]
    internal class OrleansCodeGenObserverGrainReference : global::Orleans.Runtime.GrainReference, global::IGrain.IObserverGrain
    {
        protected @OrleansCodeGenObserverGrainReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenObserverGrainReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return 1690271801;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "global::IGrain.IObserverGrain";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == 1690271801 || @interfaceId == -1277021679;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case 1690271801:
                    switch (@methodId)
                    {
                        case -1520572651:
                            return "Subscribe";
                        case -1829957677:
                            return "UnSubscribe";
                        case -1925440376:
                            return "SendUpdateMessage";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 1690271801 + ",methodId=" + @methodId);
                    }

                case -1277021679:
                    switch (@methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public global::System.Threading.Tasks.Task @Subscribe(IChat @observer)
        {
            global::Orleans.CodeGeneration.GrainFactoryBase.@CheckGrainObserverParamInternal(@observer);
            return base.@InvokeMethodAsync<global::System.Object>(-1520572651, new global::System.Object[]{@observer is global::Orleans.Grain ? @observer.@AsReference<IChat>() : @observer});
        }

        public global::System.Threading.Tasks.Task @UnSubscribe(IChat @observer)
        {
            global::Orleans.CodeGeneration.GrainFactoryBase.@CheckGrainObserverParamInternal(@observer);
            return base.@InvokeMethodAsync<global::System.Object>(-1829957677, new global::System.Object[]{@observer is global::Orleans.Grain ? @observer.@AsReference<IChat>() : @observer});
        }

        public global::System.Threading.Tasks.Task @SendUpdateMessage(global::System.String @message)
        {
            return base.@InvokeMethodAsync<global::System.Object>(-1925440376, new global::System.Object[]{@message});
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.4.0.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute(typeof (global::IGrain.IObserverGrain), 1690271801), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenObserverGrainMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            if (@grain == null)
                throw new global::System.ArgumentNullException("grain");
            switch (interfaceId)
            {
                case 1690271801:
                    switch (methodId)
                    {
                        case -1520572651:
                            return ((global::IGrain.IObserverGrain)@grain).@Subscribe((IChat)arguments[0]).@Box();
                        case -1829957677:
                            return ((global::IGrain.IObserverGrain)@grain).@UnSubscribe((IChat)arguments[0]).@Box();
                        case -1925440376:
                            return ((global::IGrain.IObserverGrain)@grain).@SendUpdateMessage((global::System.String)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 1690271801 + ",methodId=" + methodId);
                    }

                case -1277021679:
                    switch (methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return 1690271801;
            }
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.4.0.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (global::IGrain.IMongoGrain))]
    internal class OrleansCodeGenMongoGrainReference : global::Orleans.Runtime.GrainReference, global::IGrain.IMongoGrain
    {
        protected @OrleansCodeGenMongoGrainReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenMongoGrainReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return -342927010;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "global::IGrain.IMongoGrain";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == -342927010 || @interfaceId == -1277021679;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case -342927010:
                    switch (@methodId)
                    {
                        case 150038971:
                            return "Save";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -342927010 + ",methodId=" + @methodId);
                    }

                case -1277021679:
                    switch (@methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public global::System.Threading.Tasks.Task<global::System.String> @Save(global::System.String @name)
        {
            return base.@InvokeMethodAsync<global::System.String>(150038971, new global::System.Object[]{@name});
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.4.0.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute(typeof (global::IGrain.IMongoGrain), -342927010), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenMongoGrainMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            if (@grain == null)
                throw new global::System.ArgumentNullException("grain");
            switch (interfaceId)
            {
                case -342927010:
                    switch (methodId)
                    {
                        case 150038971:
                            return ((global::IGrain.IMongoGrain)@grain).@Save((global::System.String)arguments[0]).@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -342927010 + ",methodId=" + methodId);
                    }

                case -1277021679:
                    switch (methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return -342927010;
            }
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.4.0.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (global::IGrain.IStateGrain))]
    internal class OrleansCodeGenStateGrainReference : global::Orleans.Runtime.GrainReference, global::IGrain.IStateGrain
    {
        protected @OrleansCodeGenStateGrainReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenStateGrainReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return 30976365;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "global::IGrain.IStateGrain";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == 30976365 || @interfaceId == -1277021679;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case 30976365:
                    switch (@methodId)
                    {
                        case 2036766223:
                            return "StateSave";
                        case -454132849:
                            return "GetState";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 30976365 + ",methodId=" + @methodId);
                    }

                case -1277021679:
                    switch (@methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public global::System.Threading.Tasks.Task @StateSave()
        {
            return base.@InvokeMethodAsync<global::System.Object>(2036766223, null);
        }

        public global::System.Threading.Tasks.Task<global::System.String> @GetState()
        {
            return base.@InvokeMethodAsync<global::System.String>(-454132849, null);
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.4.0.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute(typeof (global::IGrain.IStateGrain), 30976365), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenStateGrainMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            if (@grain == null)
                throw new global::System.ArgumentNullException("grain");
            switch (interfaceId)
            {
                case 30976365:
                    switch (methodId)
                    {
                        case 2036766223:
                            return ((global::IGrain.IStateGrain)@grain).@StateSave().@Box();
                        case -454132849:
                            return ((global::IGrain.IStateGrain)@grain).@GetState().@Box();
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + 30976365 + ",methodId=" + methodId);
                    }

                case -1277021679:
                    switch (methodId)
                    {
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1277021679 + ",methodId=" + methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return 30976365;
            }
        }
    }
}
#pragma warning restore 162
#pragma warning restore 219
#pragma warning restore 414
#pragma warning restore 649
#pragma warning restore 693
#pragma warning restore 1591
#pragma warning restore 1998
#endif
