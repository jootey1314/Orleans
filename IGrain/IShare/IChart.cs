﻿using Orleans;

public interface IChat : IGrainObserver
{
    void ReceiveMessage(string message);
}