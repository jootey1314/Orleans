﻿using System;
using Orleans.Concurrency;

namespace IGrain.Model
{
    [Immutable] //标记此模型一旦实例化后，数据将不能再被改变
    public class GreetingData
    {
        public Guid From { get; set; }
        public string Message { get; set; }
        public int Count { get; set; }
    }
}
